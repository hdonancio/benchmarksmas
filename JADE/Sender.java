package scalability.jade;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class Sender extends Agent {
  public Writer writer;
  int count = 0;
  int rcv = 1;

  private class InformBehav extends CyclicBehaviour {

    public InformBehav(Agent a) {
      super(a);
      myAgent.doWait(50000);
    }
     
    public void action() {      
      
      // set a new message with performative
      ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
      // destinatary of message
      AID remoteAMSf = new AID("R@192.168.1.1:1099/JADE", AID.ISGUID);
      msg.addReceiver(remoteAMSf);
      // content of message
      msg.setContent("Hello World");
      
      // set a template with receiver and performative
      MessageTemplate ms = MessageTemplate.MatchSender(remoteAMSf);
      MessageTemplate mp = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
      MessageTemplate mt = MessageTemplate.and(ms, mp);

      // number of messagens sent
      count += 1;

      // get the initial time
      long start = System.currentTimeMillis();

      // send message
      send(msg);
      
      // wait for a reply
      block();

      ACLMessage reply = myAgent.receive(mt);
           
        if (reply != null) {    
          // get the final time 
          long end = System.currentTimeMillis();    
          long result = end - start;
          try {
           writer.write(Long.toString(result).concat(" " + getLocalName()+"\n"));
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
       if (count == 1000){
          try {
        writer.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
        // terminate execution
        doDelete();
        }
      }
  }

  protected void setup() {
    System.out.println("Hello World! My name is "+getLocalName());

    String my_name = getLocalName();

    // create a txt file to store RTTs
    try {
      writer = new BufferedWriter(new OutputStreamWriter(
      new FileOutputStream(my_name.concat(".txt"))));
  } catch (FileNotFoundException e) {
    e.printStackTrace();
  }

    addBehaviour(new InformBehav(this));
    
  } 
}

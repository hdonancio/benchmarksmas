package scalability.jade;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class Receiver extends Agent {

	private class RecvBehav extends CyclicBehaviour {

    public RecvBehav(Agent a) {
      super(a);
    }

    public void action() {
    	
    	// set template to consume only messages that match it
	    MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
	    
    	ACLMessage msg = myAgent.receive(mt);
    	if (msg != null) {
    		// create a reply message
    		ACLMessage reply = msg.createReply();
            reply.setPerformative(ACLMessage.INFORM);
    		reply.setContent("World Hello");
    		// send the reply message
    		send(reply);
    	}
        // wait for new messages
    	block();
    }
	}

  protected void setup() {
  	System.out.println("Hello World! My name is "+getLocalName());

	  addBehaviour(new RecvBehav(this));
  } 
}

package scalability.astra;

import astra.core.Module;
import astra.formula.Predicate;
import astra.term.Primitive;
import astra.term.Term;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;


public class Timer extends Module{
	
	public Writer writer;
	private Predicate runBelief;
	
	@SENSOR
	public boolean run() {
		runBelief = new Predicate("run", new Term[] {Primitive.newPrimitive(true)});
        agent.beliefs().addBelief(runBelief);
        return true;
	}
	
	@ACTION
	public boolean txt(String my_name) {
		try {
		    writer = new BufferedWriter(new OutputStreamWriter(
		      new FileOutputStream(my_name.concat(".txt"))));
		  } catch (FileNotFoundException e) {
		    e.printStackTrace();
		  }
		return true;
	}
	@TERM
	public long getTime(){
		long current_time = System.currentTimeMillis();
		return current_time;
	}
	
	@ACTION
	public boolean finalTime(long start, long end, String my_name) {
		long total = end - start;
		try {
	        writer.write(Long.toString(total).concat(" " + my_name+"\n"));
	      } catch (IOException e) {
	        e.printStackTrace();
	      }
		return true;
	}
	
	@ACTION
	public boolean close_txt() {
        try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
}

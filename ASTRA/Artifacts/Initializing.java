package scalability.astra;

import cartago.Artifact;
import cartago.OPERATION;

public class Initializing extends Artifact {
	
	@OPERATION
	void init() {
	defineObsProperty("starter", 0);
	}

	@OPERATION
	void inc() {
	int count = getObsProperty("starter").intValue();
	updateObsProperty("starter", count + 1);
	signal("tick");
	}
}

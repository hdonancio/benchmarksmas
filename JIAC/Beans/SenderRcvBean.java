package scalability.jiac;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;

import org.sercho.masp.space.event.SpaceEvent;
import org.sercho.masp.space.event.SpaceObserver;
import org.sercho.masp.space.event.WriteCallEvent;

import de.dailab.jiactng.agentcore.AbstractAgentBean;
import de.dailab.jiactng.agentcore.action.Action;
import de.dailab.jiactng.agentcore.comm.ICommunicationBean;
import de.dailab.jiactng.agentcore.comm.message.IJiacMessage;
import de.dailab.jiactng.agentcore.comm.message.JiacMessage;
import de.dailab.jiactng.agentcore.knowledge.IFact;
import de.dailab.jiactng.agentcore.lifecycle.LifecycleException;

public class SenderRcvBean extends AbstractAgentBean{
	
	private String AgentName;
	public Writer writer;
	
	Long start;

	@Override
	public void doStart() throws Exception {
		super.doStart();
		
		// listen to memory events, see MessageObserver implementation below
		memory.attach(new MessageObserver(), new JiacMessage(new Msg("World Hello")));
		
		AgentName = this.thisAgent.getAgentName();
		
		try {
		    writer = new BufferedWriter(new OutputStreamWriter(
		      new FileOutputStream(this.thisAgent.getAgentName().concat(".txt"))));
		  } catch (FileNotFoundException e) {
		    e.printStackTrace();
		  }
	}

	@SuppressWarnings("serial")
	private class MessageObserver implements SpaceObserver<IFact> {

		@SuppressWarnings("unchecked")
		public void notify(SpaceEvent<? extends IFact> event) {
			if (event instanceof WriteCallEvent<?>) {
				WriteCallEvent<IJiacMessage> wce = (WriteCallEvent<IJiacMessage>) event;
				// a JiacMessage holding a Ping with message 'Ping' has been 
				// written to this agent's memory
				Long end = System.currentTimeMillis();
				log.info("Sender agent - reply received");

				// consume message
				IJiacMessage message = memory.remove(wce.getObject());
				
				IFact template = new Time(null, null, null);
				IFact state = memory.read(template);
				
				//end = ((Time) state).getEnd();
				start = ((Time) state).getStart();

				long result = end - start;
				
				if(memory.read(new Stop(true)) != null) {
					try {
						writer.close();
						log.info("Execution terminated");
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					try {
						writer.write(Long.toString(result).concat(" " + AgentName +"\n"));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						}
					memory.update(state, new Time(false, null, end));
				}
			}
		}
	}
}
package scalability.jiac;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.util.List;
import java.util.Set;

import de.dailab.jiactng.agentcore.AbstractAgentBean;
import de.dailab.jiactng.agentcore.action.Action;
import de.dailab.jiactng.agentcore.comm.ICommunicationBean;
import de.dailab.jiactng.agentcore.comm.IMessageBoxAddress;
import de.dailab.jiactng.agentcore.comm.message.IJiacMessage;
import de.dailab.jiactng.agentcore.comm.message.JiacMessage;
import de.dailab.jiactng.agentcore.knowledge.IFact;
import de.dailab.jiactng.agentcore.lifecycle.LifecycleException;
import de.dailab.jiactng.agentcore.ontology.AgentDescription;
import de.dailab.jiactng.agentcore.ontology.IActionDescription;
import de.dailab.jiactng.agentcore.ontology.IAgentDescription;;

public class SenderBean extends AbstractAgentBean {

	private IActionDescription sendAction = null;
	IMessageBoxAddress receiver;
	int count = 0;
	Long start;
	String rcv;
	int num_rcv = 1;
	IFact state;
	int attempts = 0;
	//int num_ag = 40;

	@Override
	public void doStart() throws Exception {
		super.doStart();
		log.info("Sender - starting....");
		log.info("Sender - my ID: " + this.thisAgent.getAgentId());
		log.info("Sender - my Name: " + this.thisAgent.getAgentName());
		log.info("Sender - my Node: " + this.thisAgent.getAgentNode().getName());

		// Retrieve the send-action provided by CommunicationBean
		IActionDescription template = new Action(ICommunicationBean.ACTION_SEND);
		sendAction = memory.read(template);
		if (sendAction == null) {
			sendAction = thisAgent.searchAction(template);
		}

		// If no send action is available, check your agent configuration.
		// CommunicationBean is needed
		if (sendAction == null){
			throw new RuntimeException("Send action not found.");
		}
		
		memory.write(new Stop(false));
	
		//String[] n = this.thisAgent.getAgentName().split("r");
		//String r = "Receiver";
		//rcv = r.concat(n[1]);
		log.info(rcv);
		
		//nto1
		//rcv = "Receiver1";
		
		memory.write(new Time(false, null, null));								
	}

	@Override
	public void execute() {
		
		IFact template = new Time(null, null, null);
		IFact state = memory.read(template);
	
		Boolean status = ((Time) state).getBlock();
		
		System.out.println(status);
		
		if(status == false) {
			if(num_rcv < 4) {
				String r = "Receiver";
				rcv = r.concat(Integer.toString(num_rcv));
				num_rcv += 1;
			} else {
				num_rcv = 1;
			}
			
			// get the list of agents
			List<IAgentDescription> agentDescriptions = thisAgent
					.searchAllAgents(new AgentDescription());
				
			// find the partner
			for (IAgentDescription agent : agentDescriptions) {
				if (agent.getName().equals(rcv)) {
					receiver = agent.getMessageBoxAddress();
					break;
				}
			}
		
			// create the message, get receiver's  message box address
			JiacMessage message = new JiacMessage(new Msg("Hello World"));
		
			// number of messagens sent
			count += 1;
		
			// get the initial time and update it on agent's memory
			start = System.currentTimeMillis();
			memory.update(state, new Time(true, start, null));

			// invoke sendAction
			log.info("Sender - send a message!");
			invoke(sendAction, new Serializable[] { message, receiver });
				
			if (count == 1001){
				memory.write(new Stop(true));
				memory.update(state, new Time(true, null, null));
			} //else {
			//	memory.update(state, new Time(false, null, null));
			//}
		} else {
			attempts += 1;
			if (attempts == 30) {
				if(count < 1000) {
					memory.update(state, new Time(false, null, null));
					attempts = 0;
				}
			}
			
		}
	}
}

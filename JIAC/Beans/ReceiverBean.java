package scalability.jiac;

import java.io.Serializable;

import org.sercho.masp.space.event.SpaceEvent;
import org.sercho.masp.space.event.SpaceObserver;
import org.sercho.masp.space.event.WriteCallEvent;

import de.dailab.jiactng.agentcore.AbstractAgentBean;
import de.dailab.jiactng.agentcore.action.Action;
import de.dailab.jiactng.agentcore.comm.ICommunicationBean;
import de.dailab.jiactng.agentcore.comm.message.IJiacMessage;
import de.dailab.jiactng.agentcore.comm.message.JiacMessage;
import de.dailab.jiactng.agentcore.knowledge.IFact;

public class ReceiverBean extends AbstractAgentBean {

	private Action sendAction = null;

	@Override
	public void doStart() throws Exception {
		super.doStart();
		log.info("Receiver - starting....");
		log.info("Receiver - my ID: " + this.thisAgent.getAgentId());
		log.info("Receiver - my Name: " + this.thisAgent.getAgentName());
		log.info("Receiver - my Node: " + this.thisAgent.getAgentNode().getName());

		// Retrieve send action provided by CommunicationBean
		sendAction = retrieveAction(ICommunicationBean.ACTION_SEND);

		// If no send action is available, check your agent configuration.
		// CommunicationBean is needed
		if (sendAction == null)
			throw new RuntimeException("Send action not found.");

		// listen to memory events, see MessageObserver implementation below
		memory.attach(new MessageObserver(), new JiacMessage(new Msg("Hello World")));
	}

	@SuppressWarnings("serial")
	private class MessageObserver implements SpaceObserver<IFact> {

		@SuppressWarnings("unchecked")
		public void notify(SpaceEvent<? extends IFact> event) {
			if (event instanceof WriteCallEvent<?>) {
				WriteCallEvent<IJiacMessage> wce = (WriteCallEvent<IJiacMessage>) event;
				// a JiacMessage holding a Ping with message 'Ping' has been 
				// written to this agent's memory
				log.info("Receiver agent - message received");

				// consume message
				IJiacMessage message = memory.remove(wce.getObject());

				// create answer: a JiacMessage holding a Ping with message 'pong'
				JiacMessage replyMessage = new JiacMessage(new Msg("World Hello"));

				// send reply to Sender (the sender of the original message)
				log.info("Receiver - sending reply message");
				invoke(sendAction, new Serializable[] { replyMessage,
						message.getSender() });
			}
		}
	}

}

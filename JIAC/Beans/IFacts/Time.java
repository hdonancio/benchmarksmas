package scalability.jiac;

import de.dailab.jiactng.agentcore.knowledge.IFact;

public class Time implements IFact {
	private static final long serialVersionUID = 3374059561747194801L;

	
	//If block is false, then the agent can send a message
	private Boolean block ;
	private Long start;
	private Long end;
	
	public Long getStart() {
		return start;
	}
	
	public void setStart(Long time) {
		this.start = time;
	}
	
	public Long getEnd() {
		return end;
	}
	
	public void setEnd(Long time) {
		this.end = time;
	}
	
	public Boolean getBlock() {
		return block;
	}
	
	public void setBlock(Boolean state) {
		this.block = state;
	}
	
	public Time(Boolean block, Long start, Long end) {
		this.block = block;
		this.start = start;
		this.end = end;
	}
}

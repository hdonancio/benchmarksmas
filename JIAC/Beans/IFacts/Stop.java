package scalability.jiac;

import de.dailab.jiactng.agentcore.knowledge.IFact;

public class Stop implements IFact {
	private static final long serialVersionUID = 3374059561747194801L;
	
	public Boolean stop;
	
	public Stop(Boolean stop) {
		this.stop = stop;
	}

}

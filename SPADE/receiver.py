from spade import agent
from spade.behaviour import CyclicBehaviour
from spade.message import Message

class ReceiverAgent(agent.Agent):

	class RecvBehav(CyclicBehaviour):
		async def run(self):

			# wait for a message		
			msg = await self.receive()
	
			if msg:

				reply = msg.make_reply()
				reply.body = "Hello World"

				await self.send(reply)

	def setup(self):
		print("Hello World! I'm agent {}".format(str(self.jid)))
		b = self.RecvBehav()
		self.add_behaviour(b)

receiver = ReceiverAgent("receiver@lti.loc", "pedaneel")
receiver.start()

import time as time_
from spade import agent
from spade.behaviour import CyclicBehaviour
from spade.behaviour import OneShotBehaviour
from spade.message import Message

class SenderAgent(agent.Agent):

	class InformBehav(CyclicBehaviour):
		async def run(self):
			# destinatary of message
			msg = Message(to="receiver@lti.loc")
			# content of message
			msg.body = "Hello World"
			# message performative
			msg.set_metadata("performative", "inform")

			# number of messagens sent
			self.agent.count += 1
			
			# get the initial time
			start = int(round(time_.time() * 1000))

			await self.send(msg)

			# wait for a reply		
			reply = await self.receive()

			# get the final time
			end = int(round(time_.time() * 1000))

			final = end - start

			self.agent.f.write(str(final) + ' ' + str(self.agent.jid) + '\n')

			if self.agent.count == 1000:
				self.agent.stop()	

	def setup(self):
		print("Hello World!!! I'm agent {}".format(str(self.jid)))

		self.count = 0
		self.f = open(str(self.jid) + ".txt","w+")

		# add the cyclic behaviour
		b = self.InformBehav()
		self.add_behaviour(b)

# connect the agent to local server id/password
sender = SenderAgent("sender@lti.loc", "pedaneel")
sender.start()